## yggio-service-provider: listen to the publisher

Encapsulates functionality specific to an yggio service provider (connect & subscribe)

### Usage

Initialize (note that mongoose is injected as an argument)
```javascript
// set up the subscriber
const app = require('express')(); // yeah, we need an express app..
const serviceProvider = require('service-provider');

const onMessage = message => {
  doStuff(message)
};

const account = {
  name: 'owner-of-my-service',
  accessToken: 'token-gotten-using-yggio-account-package'
};

const providerConfig = {
  name: 'MNS',
  info: 'My New Service Consolidated',
  redirect_uris: {
    browser: 'https://login-redirect-example-url',
    app: 'mns://login'
  },
  channel: {
    url: 'http://my-domain.com',
    route: '/yggio-message'
  },
  logoPath: './path/to/logo.png'
};
return serviceProvider.register(
  account,
  providerConfig,
  app,
  onMessage
)
  .then(() => {
    const user = {name, accessToken};
    const node = {_id};
    serviceProvider.subscribe(user, node);
  });
```
and now the onMessage function should be working.

### License
yggio-populate-request is under the MIT license. See LICENSE file.
