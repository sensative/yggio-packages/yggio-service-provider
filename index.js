'use strict';

const _ = require('lodash');
const yggioApi = require('yggio-api');
const bodyParser = require('body-parser').json();

const subscriptionTypes = {
  IOTNODE: 'IOTNODE',
  RULE: 'RULE',
  ALARM: 'ALARM'
};

// storedSubscribe is set from the register function
const subscribe = async (user, yggioId, subscriptionType) => {
  return yggioApi.subscribe(user, {
    iotnode: yggioId,
    subscriptionType: subscriptionType || subscriptionTypes.IOTNODE
  });
};

// registering needs to be done before subscribe() will work
const register = async (serviceAccount, providerConfig) => {
  // begin registering
  const providerInfo = {
    name: providerConfig.name,
    info: providerConfig.info,
    'redirect_uri': Object.values(providerConfig.redirect_uris),
    channel: providerConfig.channel
  };
  return yggioApi.registerProvider(serviceAccount, providerInfo)
    .then(provider => {
      console.log('registered service provider: ', provider);
      // set the provider logo if appropriate
      if (providerConfig.logoPath) {
        return yggioApi.setLogo(serviceAccount, providerConfig.logoPath)
          .catch(err => console.log('setLogo error'))
          // .catch(err => console.log('setLogo err: ', err.message))
          .then(() => provider);
      }
      return Promise.resolve(provider);
    });
};

module.exports = {
  register,
  subscribe,
  subscriptionTypes
};
